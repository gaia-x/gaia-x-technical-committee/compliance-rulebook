# Resource & Subclasses

More detail about the role of a resource in the [Architecture](https://docs.gaia-x.eu/technical-committee/architecture-document/22.09/conceptual_model/#resources) document.

## Resource

A resource that may be aggregated in a Service Offering or exist independently of it.

| Attribute               | Card. | Trust Anchor | Comment                                         |
|-------------------------|-------|--------------|-------------------------------------------------|
| `aggregationOf[]`       | 0..*  | State        | `resources` related to the resource and that can exist independently of it. |
| `name`                  | 0..1     | State        | A human readable name of the data resource |
| `description`           | 0..1     | State        | A free text description of the data resource |

## Physical Resource

A Physical Resource inherits from a Resource.  
A Physical resource is, but not limited to, a datacenter, a bare-metal service, a warehouse, a plant. Those are entities that have a weight and position in physical space.

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `maintainedBy[]`       | 1..*  | State        | a list of `participant` maintaining the resource in operational condition and thus having physical access to it. |
| `ownedBy[]`            | 0..*  | State        | a list of `participant` owning the resource. |
| `manufacturedBy[]`     | 0..*  | State        | a list of `participant` manufacturing the resource. |
| `locationAddress[].countryCode` | 1..*  | State   | a list of physical locations in ISO 3166-2 alpha2, alpha-3 or numeric format. |
| `location[].gps`       | 0..*  | State        | a list of physical GPS in [ISO 6709:2008/Cor 1:2009](https://en.wikipedia.org/wiki/ISO_6709) format. |

### Sustainability
The list of the following attributes have to be linked to a service offering, physical resource or legal person.  
The aim of the attributes is to allow a customer more educated and transparent decisions regarding the environmental impact.

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `environmentalImpactReport`       | 0..1  | `ownedBy` or `providedBy`         |a resolvable link to an environmental impact report of the Provider or a link to a public database listing the organisation as a signatory of a public engagement to reach Climate Neutrality by 2030 (e.g. Climate Neutral Data Centre Pact - [signatories](https://www.climateneutraldatacentre.net/signatories/)). |
| `powerUsageEffectiveness`       | 0..1  | `ownedBy`         |Power usage effectiveness (PUE) is a ratio that describes how efficiently a computer data centre uses energy; specifically, how much energy is used by the computing equipment (in contrast to cooling and other overhead that supports the equipment). PUE is the ratio of the total amount of energy used by a computer data centre facility to the energy delivered to computing equipment. The measurement is annually according to  [ISO/IEC 30134-2:2016](https://www.iso.org/standard/63451.html) or [EN 50600-4-2](https://www.en-standard.eu/csn-en-50600-4-2-information-technology-data-centre-facilities-and-infrastructures-part-4-2-power-usage-effectiveness/).A decimal number equal or greater than 1.0 is expected. For data centres and server rooms with a planned IT capacity at or below 2MW, energy consumed for office space, ancillary building space, and general usage may be excluded from PUE calculations. For this case exclusions have to be mentioned.|
| `waterUsageEffectiveness`       | 0..1  | `ownedBy`        |Water usage effectiveness (WUE) is used to measure data centre sustainability in terms of water usage and its relation to energy consumption. It is calculated as the ratio between water used at the data centre (water loops, adiabatic towers, humidification, etc.) and energy delivered to the IT equipment. WUE will be measured using the category 1 site value, per [ISO/IEC 30134-9:2022](https://www.iso.org/standard/77692.html) standard.|
| `waterType`       | 0..*  | `ownedBy`        |Breakdown of the types of water used. Potable water is free from contamination that is safe to drink or to use for food and beverage preparation and personal hygiene, in adherence to [ISO/IEC 30134-9:2022](https://www.iso.org/standard/77692.html).Freshwater is water having a low concentration of dissolved solids, in adherence to [ISO 14046:2016](https://www.iso.org/standard/43263.html). Greywater is wastewater with a low pollution level, no faecal matter, and reuse potential, in adherence to ISO 12056-1:2000. Blackwater is wastewater with significant pollution level without reuse potential, or recycled blackwater that has gone through tertiary treatment, in adherence to ISO 12056-1:2000.Seawater or brackish is water with significant salinity, in adherence to [ISO 14046:2016](https://www.iso.org/standard/43263.html).|
| `climateType`            | 0..1  | `ownedBy`        | Cool climates are those that are at or below a cooling degree day measurement of 49.99 based on annual data in 2019 for the [NUTS 2 Region compiled by Eurostat](https://ec.europa.eu/eurostat/data/database?node_code=nrg_chddr2_a). Warm climates are those that are at or above a cooling degree day measurement of 50.00 based on annual data in 2019 for the [NUTS 2 Region compiled by Eurostat](https://ec.europa.eu/eurostat/data/database?node_code=nrg_chddr2_a). |
| `energyConsumption`            | 0..1  | `ownedBy`        | In Megawatt-Hours (MwH). |
| `renewableEnergyAmount[]`     | 0..*  | State        | Renewable is defined as technologies identified as renewable under [Directive 2009/28/EC](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX%3A32009L0028&from=EN) and carbon-free energy means any type of electricity generation from wind, solar, aerothermal, geothermal, hydrothermal and ocean energy, hydropower, biomass, landfill gas, sewage treatment plant gas, biogases, nuclear power, and carbon capture and storage. Renewable energy is measured based on the Renewable Energy Factor defined by [CSN EN 50600-4-3](https://www.en-standard.eu/csn-en-50600-4-3-information-technology-data-centre-facilities-and-infrastructures-part-4-3-renewable-energy-factor/); or a company may also measure renewable energy or carbon-free energy based on a publicly available methodology; or a company may measure renewable energy or carbon-free energy based on a published third party methodology, such as Green-e, RE100 or the Greenhouse Gas Protocol. Renewable energy can be measured at the facility, country, or company portfolio level within the Member States of the European Union. Claims should be proven by certificates (e.g. with a power purchase agreement (PPA), Guarantee of origin (GoO) or Renewable Energy Certificate (REC) and amount in MWh). |
| `dataReplication` | 0..1  | `ownedBy`   | Amount of data replications for security, back-up and other reasons. |
| `lifetimeExpectation` | 0..1  | `ownedBy`   | List of lifetime expectations of meaningful components (datacentres, buildings ...) and the methodology used in the assessment. |
| `ecolabels[]`       | 0..*  | State        | Ecolabels of equipment e.g.from but not limited to TCO or Electronic Product Environmental Assessment Tool (EPEAT). |
| `energyBandwidthEfficiency`     | 0..*  | `ownedBy`        | Break down of the bandwidth / energy consumption. |
| `environmentManagementSystem[]` | 0..*  | State   | List of certificates covering the environment management and the exact scope of it (e.g. group-wide). The list might include ISO14001, ISO14040, ISO14044, Commission Delegated Regulation (EU) 2021/2139,and the CNDCP Auditing Framework to make data centres climate neutral by 2030.|
| `lifeCycleAssessment[]`       | 0..*  | State        | Description of life cycle assessment e.g. based on ITU L.1410 or ETSI 203 199 Methodology application.This Methodology is a complement to the ISO 14040 & 14044 for environmental life cycle assessments of information and communication technology goods, networks and services. Compliance to all requirements may not be possible deviations from the requirements shall be clearly motivated and reported. |

## Virtual Resource

A Virtual Resource inherits from a Resource.  
A Virtual resource is a resource describing recorded information such as, and not limited to, a dataset, a software, a configuration file, an AI model. Special subclasses of Virtual Resource are `SoftwareResource` and `DataResource`.

```mermaid
classDiagram

VirtualResource <|-- SoftwareResource
VirtualResource <|-- DataResource 

```

| Attribute            | Card. | Trust Anchor | Comment                                   |
|----------------------|-------|--------------|-------------------------------------------|
| `copyrightOwnedBy[]` | 1..*  | State        | A list of copyright owners either as a free form string or `participant` URIs from which Self-Descriptions can be retrieved. A copyright owner is a person or organization that has the right to exploit the resource. Copyright owner does not necessarily refer to the author of the resource, who is a natural person and may differ from copyright owner. |
| `license[]`          | 1..*  | State        | A list of [SPDX](https://github.com/spdx/license-list-data/tree/master/jsonld) identifiers or URL to document|
| `policy[]`           | 1..*  | State        | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) (access control, throttling, usage, retention, ...) |

The `license` refers to the license of the virtual resource - data or software, not the license of potential instance of that virtual resource.

If there is no specified usage policy constraints on the `VirtualResource`, the  `policy` should express a simple `default: allow` intent.

## Data Resource

A `data resource` is a subclass of `virtual resource` exposed through a service instance.  
It's also known as a `Data Product` as described in the Gaia-X Data Exchange specification document.

A `data resource` is extending the [DCAT-3 Dataset class](https://www.w3.org/TR/vocab-dcat-3/#Class:Dataset) and primarily refers to an analytical dataset exposed via one or more `InstantiatedVirtualResource` service access points.

The data resource consists of the characterisation of the actual data as a description of the "contractual" part. At minimum, this self-description needs to contain all information so that a consumer can initiate a `contract negotiation`. All other attributes that are used to describe the data are optional. However, the provider has an interest to precisely describe the data so that it can be found and consumed. If the data resource is published in a catalogue, the data provider might precisely describe the data resource so that it can be found and consumed by data consumers. 

| Attribute            | Card. | Trust Anchor | Comment                                         |
|----------------------|-------|--------------|-------------------------------------------------|
| `producedBy`         | 1     | State        | a resolvable link to the participant self-description legally enabling the data usage |
| `exposedThrough[]`   | 1..*  |`producedBy`| A resolvable link to the data exchange component that exposes the data resource. |
| `obsoleteDateTime`   | 0..1  |`producedBy`| date time in ISO 8601 format after which data is obsolete. |
| `expirationDateTime` | 0..1  |`producedBy`| date time in ISO 8601 format after which data is expired and shall be deleted. |
| `containsPII`        | 1     |`producedBy`| boolean determined by Participant owning the Data Resource |
| `dataController`     | 0..*  |`producedBy`| data controller Participant as defined in GDPR. |
| `consent[]`          | 0..*  |`dataController`| list of consents covering the processing activities from the data subjects as Natural Person when the dataset contains PII. |

**Consistency rules**

- the keypair used to sign the Data Resource claims must be traceable to the `producedBy` participant of the Data Resource.
- If the data are about data subjects as one or more Natural Persons, or sensitive data as defined in GDPR [article 9](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2051-1-1), than `dataController` and `consent` are mandatory.  
  To avoid [data re-identification](https://ec.europa.eu/eurostat/cros/content/re-identification_en), this rule applies independently if the data is raw, pseudo-anonymized or anonymized. (Note: This is on purpose beyond GDPR requirements.)
- if `dataController` is specified, the keypair used to sign at least the Data Resource `consent` claims must be traceable to the `dataController`.
- Generic `authorisation` and `purpose`, not specific to PII nor consent should be expected inside the inherited `policy` attribut using ODRL [Permission](https://www.w3.org/TR/odrl-model/#permission) and ODRL [Duty](https://www.w3.org/TR/odrl-model/#duty) rules. 


### Legitimate Processing of Information Related to PII

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `legalBasis`           | 1     |`dataController`| One of the reasons as detailed in the identified Personal Data Protection Regimes, formated as a string matching `<protectionRegime>:<article number>`|
| `dataProtectionContactPoint`| 1..* |`dataController`| [ContactPoint](https://schema.org/ContactPoint) of the [Data Protection Officer](https://edps.europa.eu/data-protection/data-protection/reference-library/data-protection-officer-dpo_en) or Participant responsible for the management of personal or sensitive data |
| `purpose[]`            | 1..*  |`dataController`| Purposes of the processing. It is recommended to use well know controlled vocabulary such as the [Data Privacy Vocabulary:Purposes](https://w3c.github.io/dpv/dpv/#vocab-purpose) |
| `consentWithdrawalContactPoint`| 1..* |`dataController`| [ContactPoint](https://schema.org/ContactPoint) of the Participant to whom formulate a withdrawal consent request |

<!-- | `consentProof`         | 0     |`dataController`| Proof of the data subject consent, captured as described in the GDPR [article 7](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2001-1-1). | -->


**Consistency Rules**

- If `legalBasis` is in [`6.1.a`, `9.2.a`], then either `dataProtectionContactPoint` and `consentWithdrawalContactPoint` are mandatory.
- If `legalBasis` is not in [`6.1.a`, `9.2.a`], then `purpose` is mandatory.
<!-- - Where Data Consumers conclude with their individual risk assessment that more information is needed, i.e., in cases where the generic information on the applied legal bases may not suffice but more granular information is considered necessary, each Data Source shall provide for a dedicated contact point.  -->

<!--
The `consentProof` object shall include at least one of the following attributes:
- a certificate testifying from an interaction with the initial Natural Participant using a [RFC7517 `x5c`](https://www.rfc-editor.org/rfc/rfc7517#section-4.7) (X.509 Certificate Chain) attribute or [RFC7517 `x5u`](https://www.rfc-editor.org/rfc/rfc7517#section-4.6) (X.509 URL) attribute.  
  The `x5u` parameter should be resolvable to a `X509` `.crt`, `.pem`, `.der` or `.p7b` file which contains a valid Gaia-X Trust Anchor eligible for the signed claims of Natural Person verification.
- a URL `record` to a proof of the data subject consent, captured as described in the GDPR [article 7](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2001-1-1)

The `consentProof` endpoint might be protected with an access control mechanism. The access control logic should be computed from the self-description of the Participants requesting access to the information.
-->

<details>
  <summary>Example of <code>LegalBasis</code> value</summary>

Example for `GDPR2016` with [article 6](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e1888-1-1) or [article 9](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2051-1-1):

- `GDPR2016:6.1.a`
- `GDPR2016:6.1.4`
- `GDPR2016:9.2.j`

</details>


<details>
  <summary>Example of <code>consent</code></summary>

```json
{
  "consent": [
    {
      "consentDuration": "90d",
      "legalBasis": "6.1.b",
      "purpose": ["Account Management", "Identity Verification"]
    }
  ]
}
```
</details>

<!--
| 1.0     | `categoryOfData`       | 1     | State        | Category of data as defined in the Data Governance Act article 3 (1) and Article 3 (2). |
To be moved to a data subject consent
-->

<!--
> o       purpose,
> o       time interval,
> o       geographic scope
> o       pass through rights degrees (from 1st party through 3rd party)
> o       eventually 'delete' preference, after use
> o       eventually 'compensation' vs' non compensation requirement

-->
