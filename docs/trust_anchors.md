# Trust Anchors

The Gaia-X Compliance Service is using the same principles of Issuer / Holder / Verifier / Verifiable Data Registry as in the Verifiable Credential [model](https://www.w3.org/TR/vc-data-model/#ecosystem-overview).

![VC model](figures/vc_model.png)

The Gaia-X Compliance Service verifies:

1. that the issued claims are conformant to the format specified in this document
2. that the issuers of the information are properly registered in the Verifiable Data Registry aka Gaia-X Registry
2. that the provided information is consistent

In this model, the Issuers aka Trust Anchors can be either:

- the entity in direct control of the Trusted Data Source
- an entity recognized by Gaia-X, also known as Notary, to translate Trusted Data Sources on behalf of the entity managing the Trusted Data Source into Verifiable Credentials.

Trust Anchors are entities endorsed by Gaia-X. Trust Anchors shall underpin claims by Participants. Consequently, Trust Anchors shall facilitate the processing of claims by Participants as Trust Anchors will - subject to their fair and transparent procedures - affirm the necessary trust in otherwise mere self-declared statements. Trust Anchors may underpin any aspects, whilst Gaia-X will only be interested in those aspects  relating to criteria relevant either for the Trust Framework or the Gaia-X Labelling.

To be compliant with the Gaia-X Trust Framework, all keypairs used to sign claims must have at least one of the Trust Anchors in their certificate chain.

At any point in time, the list of valid Trust Anchors is stored in the Gaia-X Registry.

## List of defined Trust Anchors

| Name     | Defined as |
|----------|------------|
| State    | The Trust Service Providers (TSP) must be state validated identity issuers or EV SSL issuers. <br/> - For `participant`, if the `legalAddress.countryCode` is in EEA, valid state identity issuers are **eiDAS** ones. <br /> - **Gaia-X** Association may also be a valid TSP for Gaia-X Association members. |
| eiDAS    | Issuers of Qualified Certificate for Electronic Signature as defined in eIDAS [Regulation (EU) No 910/2014](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2014.257.01.0073.01.ENG) <br/> (homepage: <https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home>) <br/> (machine: <https://ec.europa.eu/tools/lotl/eu-lotl.xml>) |
| EV SSL   | Extended Validation (EV) Secure Sockets Layer (SSL) certificate issuers are considered to be temporarily valid Trust Service Providers. <br> (homepage: <https://wiki.mozilla.org/CA/Included_Certificates>) <br/> (machine: <https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReportPEMCSV>) |
| registrationNumberIssuer | During the pilot phase, the Gaia-X Association nominated itself as a valid Trust Anchor under <https://notary.gaia-x.eu> |
<!--
| EDPB CoC | List of Monitoring Bodies accreditated to the Code of Conduct approved by the [EDBP](https://edpb.europa.eu/edpb_en) <br> (list of EDBP's CoC: <https://edpb.europa.eu/our-work-tools/documents/our-documents_fr?f%5B0%5D=all_publication_type%3A61&f%5B1%5D=all_topics%3A125>) |
-->

## List of Trusted Data Sources

### For registrationNumberIssuer's Trust Anchors

For a given attribute, the Trust Anchor shall use the following Trusted Data Source:

- `EORI`: the European Commission [API](https://ec.europa.eu/taxation_customs/dds2/eos/validation/services/validation?wsdl).
- `leiCode`: the Global Legal Entity Identifier (GLEIF) [API](https://www.gleif.org/en/lei-data/gleif-api?cachepath=fr%2Flei-data%2Fgleif-api)
- `local`: the OpenCorporate [API](https://api.opencorporates.com/)
    - the returned claim will also contains information about `headquarterAddress.countryCode`
- `vatID`: for the European member states or North Ireland, the VAT Information Exchange System (VIES) [API](https://ec.europa.eu/taxation_customs/vies/checkVatTestService.wsdl)
    - the returned claim will also contains information about `headquarterAddress.countryCode` 

## Trust Anchors registration


```mermaid
sequenceDiagram
    actor issuer as Notar
    actor gx as Gaia-X
    participant vrd as Gaia-X Registry

    issuer ->> gx: submit application to be a Trust Anchor<br> for specified attributes

    critical
        gx ->> gx: verify application using Gaia-X certification schema
    option acceptance
        gx ->> vrd: register applicant as a Trust Anchor<br> for the specified attributes
        gx ->> issuer: accept application
    option rejection
        gx ->> issuer: deny application
    end
```

Gaia-X Trust Anchors will be appointed following a due process based on objective criteria (Gaia-X certification schema). The Gaia-X certification schema will be defined as soon as possible. Until the Gaia-X certification schema is being established, Gaia-X may appoint Trust Anchors on a case by case basis following transparent criteria and processes.

## Trust Anchors usage

```mermaid
sequenceDiagram
    participant wallet as Wallet
    actor holder as Participant
    actor issuer as Notar<br>(Trust Anchor)
    actor cab as Trusted Data Source


    holder ->> issuer: send unsigned claim to be verified<br> or request signed claim

    Note over issuer: The notar acts as a proxy  when the Trusted Data Source<br> cannot directly issues Verifiable Crendentials (VC)
    critical Part of the Gaia-X certification schema
        issuer ->> cab: performs automatic or manual checks<br>
    option acceptance
        cab ->> issuer: confirms the claims are legit
        issuer ->> holder: signs and emit a VC
        Note over wallet: The wallet can be self-deployed or managed,<br>onprem or remote, hardware or software.
        holder ->> wallet: store VC inside a wallet
    option rejection
        cab ->> issuer: claims cannot be verified
        issuer ->> holder: return an error
    end
```

## Compliance verification

```mermaid
sequenceDiagram
    participant wallet as Wallet
    actor holder as Participant
    participant verifier as Gaia-X Compliance
    participant vrd as Gaia-X Registry

    holder ->> wallet: fetch VC from its wallet
    wallet ->> holder: format a Verifiable Presentation (VP)
    holder ->> verifier: present the VP
    alt Perform compliance checks
        Note over verifier: Not all checks are described here
        loop For each Verifiable Claim in the VP
            verifier ->> vrd: check if the VC issuer is trusted.<br>(ie the issuer a Trust Anchor or can be<br> cryptographycally associated with one.)
            vrd -->> verifier: 
        end
    else All tests are passed
        verifier ->> holder: issues a new VC<br>(This is a Gaia-X Compliance VC)
        Note over wallet: The wallet can be self-deployed or managed,<br>onprem or remote, hardware or software.
        holder ->> wallet: store VC inside a wallet
    else At least one test failed
        verifier ->> holder: returns an error
    end
```

<!--
| Gaia-X   | As a Gaia-X association member priviledge, Gaia-X association can issue Gaia-X Compliant signed claims for organization with a valid membership. |
-->

<!--
| EV SSL   | Starting from 2022 Q3, DV SSL will be replaced by Extented Validated (EV) Secure Sockets Layer (SSL) certificate issuers. |
-->
